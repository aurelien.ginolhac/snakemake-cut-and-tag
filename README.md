

## Analysis Workflow for Cut&Tag experiment

This Snakemake template is inspired from https://github.com/maxsonBraunLab/cutTag-pipeline


It used a specific peak caller for Cut&Tag: [gopeaks](https://github.com/maxsonBraunLab/gopeaks) and published in [Genome Biol](https://doi.org/10.1186/s13059-022-02707-w)


## TODO

- Markdup?
- Mapping quality filtering? Not done in `gopeaks` AFAIK