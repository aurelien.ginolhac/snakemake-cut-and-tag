FROM eddelbuettel/r2u:jammy

LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"

# build with docker build -f Dockerfile -t ginolhac/snake-cut-and-tag:0.1 .
# docker push ginolhac/snake-cut-and-tag:0.1

LABEL version="0.1"
LABEL description="Docker image to build the snakemake cut&tag singularity"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:openjdk-r/ppa && \
    apt-get update && apt-get install -y \
        bwa \
        build-essential \
        cmake \
        curl \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl libgd-graph-perl \
        openjdk-8-jdk \
        python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

# default to python3
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.8-config && ln -sf /usr/bin/python3 /usr/bin/python

# htslib
ARG HTS_VERSION="1.16"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/htslib/releases/download/${HTS_VERSION}/htslib-${HTS_VERSION}.tar.bz2 && \
    tar -xf htslib-${HTS_VERSION}.tar.bz2 && rm htslib-${HTS_VERSION}.tar.bz2 && cd htslib-${HTS_VERSION} && \
    ./configure --enable-libcurl --enable-s3 --enable-plugins --enable-gcs && \
    make && make install && make clean


# samtools
ARG SAM_VERSION="1.16"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/samtools/releases/download/${SAM_VERSION}/samtools-${SAM_VERSION}.tar.bz2 && \
    tar -xf samtools-${SAM_VERSION}.tar.bz2 && rm samtools-${SAM_VERSION}.tar.bz2 && cd samtools-${SAM_VERSION} && \
    ./configure --with-htslib=/opt/htslib-${HTS_VERSION} && make && make install && make clean


# bedtools
ARG BT_VERSION="2.30.0"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/arq5x/bedtools2/releases/download/v${BT_VERSION}/bedtools-${BT_VERSION}.tar.gz && \
    tar -xf bedtools-${BT_VERSION}.tar.gz && rm bedtools-${BT_VERSION}.tar.gz && \
    cd bedtools2 && make && make install && make clean

# AdapterRemoval
ARG AR_VERSION="2.3.3"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/MikkelSchubert/adapterremoval/archive/v${AR_VERSION}.tar.gz && \
    tar -xf v${AR_VERSION}.tar.gz && rm v${AR_VERSION}.tar.gz && \
    cd adapterremoval-${AR_VERSION} && make && make install && make clean


# Bowtie2
ARG BT2_VERSION="2.5.0"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/BenLangmead/bowtie2/releases/download/v${BT2_VERSION}/bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    unzip bowtie2-${BT2_VERSION}-linux-x86_64.zip && rm bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    mv bowtie2-${BT2_VERSION}-linux-x86_64/bowtie2* /usr/local/bin/ && rm -rf bowtie2-${BT2_VERSION}-linux-x86_64

# FastScreen
ENV PERL_MM_USE_DEFAULT=1
ARG FQ_SCREEN_VERSION="0.15.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/StevenWingett/FastQ-Screen/archive/refs/tags/v${FQ_SCREEN_VERSION}.tar.gz && \
    tar -xf v${FQ_SCREEN_VERSION}.tar.gz && rm v${FQ_SCREEN_VERSION}.tar.gz
ENV PATH /opt/FastQ-Screen-${FQ_SCREEN_VERSION}:$PATH

# FastQC
RUN cd /opt && \
    wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip && \
    unzip fastqc_v0.11.9.zip && rm fastqc_v0.11.9.zip && chmod +x FastQC/fastqc
ENV PATH /opt/FastQC:$PATH

RUN mkdir /opt/ucsc && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bigWigToBedGraph && \
    wget --no-check-certificate -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig && \
    chmod 755 /opt/ucsc/*
ENV PATH /opt/ucsc:$PATH

# gopeaks
ARG GOPEAKS_VERSION="1.0.0"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/maxsonBraunLab/gopeaks/releases/download/v${GOPEAKS_VERSION}/gopeaks-linux-amd64 && \
    mv gopeaks-linux-amd64 /usr/local/bin/gopeaks && chmod +x /usr/local/bin/gopeaks

# Preseq
ARG PRESEQ_VERSION="3.2.0"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/smithlabcode/preseq/releases/download/v${PRESEQ_VERSION}/preseq-${PRESEQ_VERSION}.tar.gz && \
    tar -xf preseq-${PRESEQ_VERSION}.tar.gz && rm preseq-${PRESEQ_VERSION}.tar.gz && \
    cd preseq-${PRESEQ_VERSION} && ./configure --prefix=/usr/local/ && make && make install && make clean

# HOMER
RUN mkdir /opt/HOMER && cd /opt/HOMER && \
    wget --no-check-certificate http://homer.ucsd.edu/homer/configureHomer.pl && \
    perl configureHomer.pl -install
ENV PATH /opt/HOMER/bin:$PATH


# python modules
RUN python3 -m pip install snakemake multiqc pandas pyBigWig deeptools

RUN install.r ggplot2 hexbin ggrepel cowplot stringr \
  deseq2 apeglm

    # clean up
#RUN apt-get clean && \
#    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
#    apt-get autoclean && \
#    apt-get autoremove -y && rm -rf /var/lib/{dpkg,cache,log}/ # keep /var/lib/apt


