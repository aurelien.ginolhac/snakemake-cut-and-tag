import pandas as pd
import numpy as np
import tempfile
from snakemake.utils import validate, min_version
##### set minimum snakemake version #####
min_version("5.20.1") # for singularity binds
from snakemake.utils import validate, min_version

##### load config and sample sheets #####
configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

samples = pd.read_table(config["samples"]).set_index("sample", drop=False)
validate(samples, schema="../schemas/samples.schema.yaml")

units = pd.read_table(config["units"], dtype=str).set_index(["sample", "unit"], drop=False)
units.index = units.index.set_levels([i.astype(str) for i in units.index.levels])  # enforce str in index
validate(units, schema="../schemas/units.schema.yaml")


# check if user didn't change the headers and/or add extra columns
assert units.columns.values.flatten().tolist() == ['sample', 'unit', 'fq1', 'fq2', 'peaks'], "config/units.tsv columns header must be 'sample', 'unit', 'fq1', 'fq2', 'peaks'"
assert samples.columns.values.flatten().tolist() == ['sample', 'condition'], "config/samples.tsv columns header must be 'sample', 'condition'"

assert len(np.unique(units["fq1"].values).tolist()) == len(units), "In units.tsv, some fq1 path are duplicated"
# check that sample are all different when not technical replicates
sample_dup=units[units.duplicated(['sample'], keep=False)]
assert len(np.unique(sample_dup["unit"].values).tolist()) == len(sample_dup), "sample {} in units.tsv is duplicated".format(sample_dup["sample"].values.tolist())
# fq2 should NOT be one of the fq1
assert not units["fq2"].isin(units["fq1"]).values.any(), "fq2 files cannot be found in fq1"

# check if sample in units.tsv and samples.tsv are coherent
for usp in np.unique(units["sample"].values).tolist():
  assert usp in np.unique(samples["sample"].values).tolist(), "sample {} in units.tsv is not samples.tsv".format(usp)
for ssp in np.unique(samples["sample"].values).tolist():
  assert ssp in np.unique(units["sample"].values).tolist(), "sample {} in samples.tsv is not units.tsv".format(ssp)
# check for paired-end that FASTQ are different
for usp in np.unique(units["sample"].values).tolist():
    fq2=units.loc[usp, ["fq2"]].values.flatten().tolist()
    if not fq2 is None:
        assert fq2 != units.loc[usp, ["fq1"]].values.flatten().tolist(), "sample {} in units.tsv have identical fq1 and fq2 files!".format(usp)


##### wildcard constraints #####

wildcard_constraints:
    sample = "|".join(samples.index),
    unit = "|".join(units["unit"])

def get_settings(sample, unit):
    if config["trimming"]["skip"]:
        # no trimming, don't look for setting files
        # return an empty list
        return []
    else:
        if not is_single_end(sample):
            # paired-end sample
            return expand("trimmed_pe/{sample}-{unit}.settings", sample=sample, unit=unit)
        # single end sample
        raise ValueError(f"Sample {sample}, is single-end. Only paired-end is accepted")


def get_peak_type(wildcards):
    ptype=units.loc[(wildcards.sample, wildcards.unit), ["peaks"]].peaks
    return "--broad --mdist 3000" if ptype == "broad" else ""


def get_raw_fq(wildcards):
    # no trimming, use raw reads
    return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()


def get_deseq2_threads(wildcards=None):
    # https://twitter.com/mikelove/status/918770188568363008
    few_coeffs = False if wildcards is None else len(get_contrast(wildcards)) < 10
    return 1 if len(samples) < 100 or few_coeffs else 6

def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


def is_single_end(sample):
    fq2=pd.isnull(units.loc[(sample), ["fq2"]].values.flatten().tolist())
    if len(fq2) > 1:
        assert len(np.unique(fq2)) == 1, "units per sample are expected to of same seq type (se/pe)"
    return np.unique(fq2)


def get_fastq(wildcards):
    fastqs = units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
    if len(fastqs) == 2:
        return {"fq1": fastqs.fq1, "fq2": fastqs.fq2}
    return {"fq1": fastqs.fq1}


# return list of technical replicates (units) for fq1 if present
def get_fq1(sample):
    if config["trimming"]["skip"]:
        # no trimming, use raw reads
        return units.loc[(sample), ["fq1"]].values.flatten().tolist() 
    else:
        # yes trimming, use trimmed data
        if not is_single_end(sample):
            # paired-end sample
            return expand("trimmed_pe/{sample}-{unit}.1.fastq.gz", sample=sample, unit=units.loc[(sample), ["unit"]].values.flatten())
        # single end sample, return only 1 file using pure python format
        return expand("trimmed_se/{sample}-{unit}.fastq.gz", sample=sample, unit=units.loc[(sample), ["unit"]].values.flatten())

# return list of technical replicates (units) for fq1 if present
def get_fq2(sample):
    if config["trimming"]["skip"]:
        # no trimming, use raw reads
        return units.loc[(sample), ["fq2"]].values.flatten().tolist() 
    else:
        # yes trimming, use trimmed data
        if not is_single_end(sample):
            # paired-end sample
            return expand("trimmed_pe/{sample}-{unit}.2.fastq.gz", sample=sample, unit=units.loc[(sample), ["unit"]].values.flatten())
        # single end sample, return None
        return []

