
rule multiqc:
    input:
        [get_settings(unit.sample, unit.unit) for unit in units.itertuples()],
        expand("qc/fastqc/{unit.sample}-{unit.unit}_fastqc.zip", unit=units.itertuples()),
        expand("qc/fastq_screen/{unit.sample}-{unit.unit}.fastq_screen.txt", unit=units.itertuples())
    output:
        "qc/multiqc_report.html"
    log:
        "logs/multiqc.log"
    wrapper:
        "v1.21.2/bio/multiqc"

rule fastqc:
    input:
        sample=get_raw_fq
    output:
        html="qc/fastqc/{sample}-{unit}.html",
        zip="qc/fastqc/{sample}-{unit}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
    group: "raw_qc"
    params: "--quiet"
    log:
        "logs/fastqc/{sample}-{unit}.log"
    wrapper:
        "v1.21.2/bio/fastqc"

rule fastq_screen:
    input:
        sample=get_raw_fq
    output:
        txt="qc/fastq_screen/{sample}-{unit}.fastq_screen.txt",
        png="qc/fastq_screen/{sample}-{unit}.fastq_screen.png"
    group: "raw_qc"
    log:
        "logs/fastq_screen/{sample}-{unit}.log"
    params:
        fastq_screen_config = {
            'database': {
                'human': {
                  'bowtie2': "{}/Human/Homo_sapiens.GRCh38".format(config["params"]["db_bowtie_path"])},
                'mouse': {
                    'bowtie2': "{}/Mouse/Mus_musculus.GRCm38".format(config["params"]["db_bowtie_path"])},
                'rat':{
                  'bowtie2': "{}/Rat/Rnor_6.0".format(config["params"]["db_bowtie_path"])},
                'drosophila':{
                  'bowtie2': "{}/Drosophila/BDGP6".format(config["params"]["db_bowtie_path"])},
                'worm':{
                  'bowtie2': "{}/Worm/Caenorhabditis_elegans.WBcel235".format(config["params"]["db_bowtie_path"])},
                'yeast':{
                  'bowtie2': "{}/Yeast/Saccharomyces_cerevisiae.R64-1-1".format(config["params"]["db_bowtie_path"])},
                'arabidopsis':{
                  'bowtie2': "{}/Arabidopsis/Arabidopsis_thaliana.TAIR10".format(config["params"]["db_bowtie_path"])},
                'ecoli':{
                  'bowtie2': "{}/E_coli/Ecoli".format(config["params"]["db_bowtie_path"])},
                'rRNA':{
                  'bowtie2': "{}/rRNA/GRCm38_rRNA".format(config["params"]["db_bowtie_path"])},
                'MT':{
                  'bowtie2': "{}/Mitochondria/mitochondria".format(config["params"]["db_bowtie_path"])},
                'PhiX':{
                  'bowtie2': "{}/PhiX/phi_plus_SNPs".format(config["params"]["db_bowtie_path"])},
                'Lambda':{
                  'bowtie2': "{}/Lambda/Lambda".format(config["params"]["db_bowtie_path"])},
                'vectors':{
                  'bowtie2': "{}/Vectors/Vectors".format(config["params"]["db_bowtie_path"])},
                'adapters':{
                  'bowtie2': "{}/Adapters/Contaminants".format(config["params"]["db_bowtie_path"])},
                'mycoplasma':{
                  'bowtie2': "{}/Mycoplasma/mycoplasma".format(config["params"]["db_bowtie_path"])}
                 },
                 'aligner_paths': {'bowtie2': "{}/bowtie2".format(config["params"]["bowtie_path"])}
                },
        subset=100000,
        aligner='bowtie2'
    threads: 8
    wrapper:
        "v1.21.2/bio/fastq_screen"

rule session:
    output:
        report("results/session.txt", category = "Versions", caption = "../report/session.rst")
    log:
        "logs/session.log"
    script:
        "../scripts/session.R"

