
rule bowtie2_index:
    input:
        ref="refs/{}.fasta".format(config["ref"]["build"])
    output:
        multiext(
            "refs/{}.fasta".format(config["ref"]["build"]),
            ".1.bt2",
            ".2.bt2",
            ".3.bt2",
            ".4.bt2",
            ".rev.1.bt2",
            ".rev.2.bt2",
        )
        #directory("refs/{}.fasta".format(config["ref"]["build"]))
    message:
        "Bowtie2 indexing {}".format(config["ref"]["build"])
    log:
        "logs/bowtie2_index_{}.log".format(config["ref"]["build"])
    threads: 8
    params:
        extra=""
    wrapper:
        "v1.21.2/bio/bowtie2/build"


rule bowtie2:
    input:
        reads=["trimmed_pe/{sample}-{unit}.1.fastq.gz", "trimmed_pe/{sample}-{unit}.2.fastq.gz"],
        folder=rules.bowtie2_index.output,
        idx="refs/{}.fasta".format(config["ref"]["build"])
    output:
        temp("mapped/{sample}-{unit}.bam")  # ,[0-9]+ contrainst wildcard unit, takes _sort otherwise
    log:
        "logs/bowtie2/{sample}-{unit}.log"
    params:
        extra=""
    threads: 4
    shell:
        "bowtie2 --local --very-sensitive-local "
        "--no-unal --no-mixed --threads {threads} "
        "--no-discordant --phred33 "
        "-I 10 -X 700 -x {input.idx} "
        "-1 {input.reads[0]} -2 {input.reads[1]} 2> {log} | samtools view -Sbh - > {output}"

rule samtools_sort:
    input:
        "mapped/{sample}-{unit}.bam"
    output:
        "mapped/{sample}-{unit}_sort.bam"
    log:
        "logs/samtools_sort/{sample}-{unit}.log"
    params:
        extra="-m 4G",  # optional params string
        tmp=tempfile.TemporaryDirectory().name+"sort_bam"
    threads: 2
    shell:
        "samtools sort {params.extra} -@ {threads} -T {params.tmp} -o {output} {input} 2> {log}"

rule samtools_index:
    input:
        "mapped/{sample}-{unit}_sort.bam"
    output:
        "mapped/{sample}-{unit}_sort.bam.bai"
    log:
        "logs/samtools_index/{sample}-{unit}.log"
    params:
        extra="",  # optional params string
    threads: 3  # This value - 1 will be sent to -@
    wrapper:
        "v1.21.2/bio/samtools/index"
