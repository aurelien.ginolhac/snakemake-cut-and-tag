rule callpeaks:
    input:
        "mapped/{sample}-{unit}_sort.bam"
    output:
        "results/callpeaks/{sample}-{unit}_peaks.bed"
    log:
        "logs/callpeaks/{sample}-{unit}_gopeaks.log"
    params:
        params=get_peak_type,
        outfile = "results/callpeaks/{sample}-{unit}"
    shell:
        "gopeaks -b {input} -o {params.outfile} {params.params} > {log} 2>&1"

rule bigwigs:
    input:
        bam="mapped/{sample}-{unit}_sort.bam",
        idx="mapped/{sample}-{unit}_sort.bam.bai"
    output:
        "results/bigwigs/{sample}-{unit}.bw"
    log:
        "logs/deeptools/{sample}-{unit}_bamcoverage.log"
    threads: 4
    params:
        params = "--binSize 10 --normalizeUsing BPM --ignoreForNormalization chrX chrY"
    shell:
        "bamCoverage --bam {input.bam} -p {threads} -o {output} {params.params} > {log} 2>&1"
